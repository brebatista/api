<?php
/**
 * Clase para el manejo de Authorizacion de la app con el API de Spotify
 *
 * @package SpotifyAuthorization.php
 * @author Brenda Batista B. - brebatista@gmail.com
 *
 */
namespace App\Http\Sources;

use App\Http\Sources\SpotifyException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client;

class SpotifyAuthorization
{
    private $clientId;
    private $clientSecret;

    private $token;
    private $tokenType;
    private $expirationDate;
    private $scope;

    public function __construct($clientId, $clientSecret)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * Funcion que obtiene el token de acceso para hacer peticiones a la API
     *
     * @throws SpotifyException
     */
    private function getTokenAPI(): void
    {    
        $spotifyClient = New Client();
        try {
            //  Creamos la petición para autorizar la aplicación y obtener el token de acceso
            $response = $spotifyClient->post(config('spotify.spotify_url_api_token'), [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Accepts' => 'application/json',
                    'Authorization' => 'Basic '.base64_encode($this->clientId.':'.$this->clientSecret),
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                ],
            ]);
        } catch (RequestException $e) {
            $errorResponse = json_decode($e->getResponse()->getBody()->getContents());
            $status = $e->getCode();
            $message = $errorResponse->error_description;

            throw new SpotifyException($message, $status, $errorResponse);
        }

        $body = json_decode((string) $response->getBody());

        $this->token = $body->access_token;
        $this->tokenType = $body->token_type;
        $this->expirationDate = Carbon::now()->addSeconds($body->expires_in);
        $this->scope = $body->scope;
    }

    /**
     * Funcion para obtener el token de acceso a la api de spotify
     *
     * @return string
     * @throws SpotifyException
     */
    public function getDataToken(): string
    {
        //  Si no tenemos un token generamos uno
        if (! $this->hasToken()) {
            $this->getTokenAPI();
        }

        //  Si el token expiró generamos un token nuevo
        if ($this->TokenHasExpired()) {
            $this->getTokenAPI();
        }

        return $this->token;
    }

    /**
     * Valida si el token de acceso ha expirado
     *
     * @return bool
     */
    private function TokenHasExpired(): bool
    {
        if (isset($this->expirationDate) && $this->expirationDate->isPast()) {
            return true;
        }

        return false;
    }

    /**
     * Valida si hay un token activo
     *
     * @return bool
     */
    private function hasToken(): bool
    {
        if (isset($this->token)) {
            return true;
        }

        return false;
    }
}