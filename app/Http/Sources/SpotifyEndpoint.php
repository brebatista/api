<?php
/**
* Clase para generar las peticiones al enpoint de spotify
*
* @package SpotifyEnpoint.php
* @author Brenda Batista B. - brebatista@gmail.com
*
*/
namespace App\Http\Sources;;

use App\Http\Sources\SpotifyException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class SpotifyEndpoint
{
    private $token;

    /**
     * Constructor de la clase
     *
     * @param string $token Token de autenticacion 
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Funcion que genera la petición al endpoint
     *
     * @param string $endpoint Nombre del endpoint
     * @param array $params Parametros del endpoint
     * @return array
     * @throws SpotifyException
     */
    public function getEndpoint(string $endpoint, array $params = []): array
    {
        $spotifyClient = New Client();
        try {
            //  Generamos la petición al endpoint
            $response = $spotifyClient->get(config('spotify.spotify_url_endpoint').$endpoint.'?'.http_build_query($params), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accepts' => 'application/json',
                    'Authorization' => 'Bearer '.$this->token,
                ],
            ]);
        } catch (RequestException $e) {
            $errorResponse = json_decode($e->getResponse()->getBody()->getContents());
            $status = $errorResponse->error->status;
            $message = $errorResponse->error->message;

            throw new SpotifyException($message, $status, $errorResponse);
        }

        return json_decode((string) $response->getBody(), true);
    }
}
