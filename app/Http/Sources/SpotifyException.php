<?php
/**
* Clase para instanciar excepciones
*
* @package SpotifyException.php
* @author Brenda Batista B. - brebatista@gmail.com
*
*/
namespace App\Http\Sources;

class SpotifyException extends \Exception
{
    
}
