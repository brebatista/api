<?php
/**
* Controlador para las funciones del endpoint
*
* @package SpotifyController.php
* @author Brenda Batista B. - brebatista@gmail.com
*
*/

namespace App\Http\Controllers;

use App\Http\Sources\SpotifyEndpoint;
use App\Http\Sources\SpotifyAuthorization;


class SpotifyController extends Controller
{

    private $token;
    private $endpoint;

    /**
     * Constructor de la clase
     */
    public function __construct()
    {
        //  Creamos el objeto que gestiona la autorizacion al api
        $SpotifyAuth = new SpotifyAuthorization(config('spotify.appDataAuth.client_id'), config('spotify.appDataAuth.client_secret'));
        $this->token = $SpotifyAuth->getDataToken();

        //  Creamos el objeto para acceder al endpoint de spotify 
        $this->endpoint = New SpotifyEndpoint($this->token);
    }

    /**
     * Funcion para obtener los albumnes de un artista
     *
     * @return json 
     */
    public function getAlbums()
    {        
        $spotifyIDArtist = $this->getIDArtist($this->token);
        $params = [];
        //  Validamos el Id del artista
        if ($spotifyIDArtist != "") {
            $albums = $this->endpoint->getEndpoint("artists/".$spotifyIDArtist."/albums", $params);
            foreach ($albums['items'] as $value) {
                $item = [];
                $item['name'] = $value['name'];
                $item['released'] = date("d-m-Y", strtotime($value['release_date']));
                $item['tracks'] = $value['total_tracks'];
                $item['cover'] = $value['images'][0];
                $items[] = $item;
            }            
            return response()->json($items);

        } else {
            return response()->json(["No se encuentra un artista con este nombre"]);
        }
    }

    /**
     * Funcion que obtienes el ID del artista en spotify
     *
     * @param string $token Token autorizado de acceso a la api
     * @return string $resp Id del artista, si este fue encontrado
     */
    public function getIDArtist($token)
    {
        $resp = "";   
        if (isset($_GET['q']) && !empty($_GET['q'])) {
            $band_name = $_GET['q'];
        } else {
            return $resp;
        }
        //  Validamos que el token no esté vacío
        if (!empty($token)) {
            $params = [
                    'q' => $band_name,
                    'type' => 'artist',
            ];
           //   Generamos la petición al endpoint
            $response = $this->endpoint->getEndpoint("search", $params);
            //  Si tenemos resultados
            if (count($response['artists']['items']) > 0) {
                $resp =  $response['artists']['items'][0]['id'];
            } 
        }
        return $resp; 
    }
}
