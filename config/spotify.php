<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client ID y Client Secret de su Spotify App.
    |--------------------------------------------------------------------------
    |
    */
    'appDataAuth' => [
        'client_id' => env('SPOTIFY_APP_CLIENT_ID'),
        'client_secret' => env('SPOTIFY_APP_CLIENT_SECRET'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Url para obtener el token de autenticacion de la API
    |--------------------------------------------------------------------------
    |
    */
    'spotify_url_api_token' => 'https://accounts.spotify.com/api/token',

    /*
    |--------------------------------------------------------------------------
    | Url para obtener los endpoints de la API
    |--------------------------------------------------------------------------
    |
    */
    'spotify_url_endpoint' => 'https://api.spotify.com/v1/',


];
