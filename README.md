## Acerca de este proyecto

El objetivo de este proyecto es crear un endpoint,  utilizando la api de spotify,  al que ingresando el nombre de la banda se obtenga un array de toda la discografia, cada disco debe tener este formato:
[{
    "name": "Album Name",
    "released": "10-10-2010",
     "tracks": 10,
     "cover": {
         "height": 640,
         "width": 640,
         "url": "https://i.scdn.co/image/6c951f3f334e05ffa"
     }
 },
  ...
]

El url del endpoint es el siguiente:

http://localhost/api/v1/albums?q=band-name


## Tecnologías utilizadas

- Laravel 6.16
- Guzzlehttp 6.5
- Spotify WEB API

## Instalación

Clonar el repositorio:

-  git clone https://gitlab.com/brebatista/api

Ubicarse dentro de la carpeta del proyecto e Instalar las librerías:

- composer install

Crear archivo de configuracion

- Copiar archivo .env.example y renombrar a .env

Crear key para la aplicación:

- php artisan key:generate


## Configuración 

Variables de entorno de spotify (Archivo .env):

- SPOTIFY_APP_CLIENT_ID = CLIENT_ID
- SPOTIFY_APP_CLIENT_SECRET = CLIENT_SECRET
