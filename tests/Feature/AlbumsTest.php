<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AlbumsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_albumsTest()
    {
        $band_name = "gorillaz";
        $response = $this->get('/albums?q='.$band_name);
        
        $response->assertStatus(200);
    }
}
